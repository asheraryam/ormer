tool

extends Node2D

export (int,0,20) var depth = 2
export (float,0,100) var radius = 20
export (float,0,100) var gap = 5
export (float,0,100) var radius2 = 5
export (bool) var generate = false setget _generate

var tmpl = null
var slot = null

func _generate(g):
	generate = false
	if g:
		do_generate()

func add_hexa( tmpl, id ):
	var sprite = tmpl.duplicate()
	sprite.name = "hexa_" + str(id)
	add_child(sprite)
	sprite.set_owner(get_tree().get_edited_scene_root())
	return sprite

func add_slot( tmpl, id ):
	var sprite = tmpl.duplicate()
	sprite.name = "slot_" + str(id)
	add_child(sprite)
	sprite.set_owner(get_tree().get_edited_scene_root())
	return sprite

func do_generate():
	
	while( get_child_count() > 0 ):
		remove_child( get_child(0) )
	
	var tex = load( "res://hexa_256.png" )
	tmpl = Sprite.new()
	tmpl.texture = tex
	tmpl.scale = Vector2( radius, radius ) / tex.get_size()
	
	tex = load( "res://hexa_ori_256.png" )
	slot = Sprite.new()
	slot.texture = tex
	slot.modulate = Color(1,0,0,1)
	slot.scale = Vector2( radius2, radius2 ) / tex.get_size()
	
	var r = 0
	for i in range( 0, depth + 1 ):
		
		var counter = 0
		
		if i == 0:
			add_hexa( tmpl, str(i)+"|"+str(counter) )
			counter += 1
		
		else:
			var a_offset = PI / 6
			for j in range( 0, 6 ):
				var a = a_offset + PI * j / 3
				var start = Vector2( cos(a), sin(a) ) * r
				a += PI / 3
				var dir = ( Vector2( cos(a), sin(a) ) * r - start ) / i
				for h in range( 0, i ):
					var sprite = add_hexa( tmpl, str(i)+"|"+str(counter) )
					counter += 1
					sprite.position = start + dir * h
		
		r += radius * sin(PI/3) + gap
	
	r = radius / 2
	var rr = ( radius * sin(PI/3) + gap ) * 0.5
	var counter = 0
	for j in range( 0, depth + 1 ):
		if j > -1:
			var a = PI / 6
			var rot = 0
			for i in range( 0, 6 ):
				var div = j * 4 + 2
				var start = Vector2( cos(a), sin(a) ) * rr
				a += PI / 3
				var seg = ( Vector2( cos(a), sin(a) ) * rr - start ) / div
				var dir = seg.normalized()
				var perp = Vector2( dir.y, -dir.x )
				counter += 1
				var jj = 0
				for j in range(1,div,2):
					var s = add_slot( slot, counter )
					s.position = start + seg * j
					if jj % 2 == 0:
						s.position += perp * (radius+gap) * (1 - sin(PI/3))
					else:
						s.position -= perp * (radius+gap) * (1 - sin(PI/3))
					s.rotation = rot
					counter += 1
					jj += 1
					rot += PI / 3
		
		rr += radius * sin(PI/3) + gap

func _ready():
	pass

func _process(delta):
	pass
