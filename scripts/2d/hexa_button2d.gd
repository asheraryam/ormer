tool

extends StaticBody2D

signal hexa_pressed

export (float, 0, 100) var radius = 1
export (float, 0, 100) var gap = 5 # used for submenus
export (String) var _text = "BUTTON" setget text
export (bool) var horizontal = false
export (bool) var _generate = false setget generate
export (bool) var regenerate = false

export (bool) var disabled = false setget disable
export (bool) var selectable = true
export (bool) var reactive = true
export (bool) var selected = false setget select
export (bool) var submenu_visible = false

export (Dictionary) var start_style = null
export (Dictionary) var normal_style = null
export (Dictionary) var hover_style = null
export (Dictionary) var selected_style = null
export (Dictionary) var disabled_style = null

onready var init_txt = _text
onready var current_style = null
onready var target_style = null
onready var hovered = false

onready var subparts = [ $border, $mesh, $coll, $txt ]
var submenu = []

################## EDITOR

func generate( b ):
	_generate = false
	disable( disabled )
	if b:
		var pts = PoolVector2Array()
		var start_at = 1
		if horizontal:
			start_at = 0
		
		for i in range(start_at,12,2):
			var a = TAU / 12 * i
			var v2 = Vector2( cos(a) * radius, sin(a) * radius )
			pts.append( v2 )
		$mesh.polygon = pts
		$coll.polygon = pts
		$border.polygon = pts
		
		$txt.rect_size = Vector2( radius * 2, $txt.rect_size.y)
		$txt.rect_position = Vector2( -radius, $txt.rect_size.y * -0.5 )
		# setting text
		text( _text )
		# setting colors
		$mesh.color = normal_style.bg
		$txt.set( "custom_colors/font_color", normal_style.txt )
	
	generate_styles()

func generate_styles():
	start_style = validate_style( start_style )
	normal_style = validate_style( normal_style )
	hover_style = validate_style( hover_style )
	selected_style = validate_style( selected_style )
	disabled_style = validate_style( disabled_style )

################## INGAME

#####################
# status management #
#####################

func text( t ):
	if t == null:
		if init_txt == null:
			init_txt = "blank"
		t = init_txt
	_text = t
	if $txt != null:
		$txt.text = _text

func select( b ):
	selected = b
	if selected:
		target_style = selected_style
	elif hovered:
		target_style = hover_style
	else:
		target_style = normal_style

func disable( b ):
	disabled = b
	if disabled:
		target_style = disabled_style
		hovered = false
		selected = false
	else:
		select( selected )

#####################
# styles management #
#####################

func validate_style( style ):
	var tmp = new_style()
	if style == null:
		return tmp
	for k in tmp:
		if not k in style:
			style[k] = tmp[k]
	return style

func new_style():
	return {
		'border' : Color(1,1,1,1),
		'bscale' : Vector2(1,1),
		'bg' : Color(1,1,1,1),
		'txt' : Color(0,0,0,1),
		'scale' : Vector2(1,1),
		'speed' : 10
	}

func cp_styles( template ):
	start_style = template.start_style.duplicate( true )
	normal_style = template.normal_style.duplicate( true )
	hover_style = template.hover_style.duplicate( true )
	selected_style = template.selected_style.duplicate( true )
	disabled_style = template.disabled_style.duplicate( true )

func mix_style( src, alpha ):
	current_style.border += ( src.border - current_style.border ) * alpha
	current_style.bg += ( src.bg - current_style.bg ) * alpha
	current_style.txt += ( src.txt - current_style.txt ) * alpha
	current_style.scale += ( src.scale - current_style.scale ) * alpha
	current_style.bscale += ( src.bscale - current_style.bscale ) * alpha
	if (current_style.scale - src.scale).length() < 0.01 and (current_style.bscale - src.bscale).length() < 0.01 and ( current_style.border - src.border ).gray() and ( current_style.bg - src.bg ).gray() < 0.01 and ( current_style.txt - src.txt ).gray() < 0.01:
		current_style = target_style.duplicate( true )

func apply_style( style ):
	if style == null:
		print( 'apply_style :: empty style' )
		return
	$mesh.scale = style.scale
	$mesh.color = style.bg
	$border.color = style.border
	$border.scale = style.bscale
	$txt.set( "custom_colors/font_color", style.txt )

func compare_styles( dst, src ):
	if dst == null or src == null:
		return false
	return dst.scale == src.scale and dst.bg == src.bg and dst.txt == src.txt and dst.speed == src.speed

func restart():
	current_style = start_style.duplicate( true )
	apply_style( current_style )
	disable( disabled )
	for s in submenu:
		if s.node.visible and s.is_hexa:
			s.node.restart()

################
# system calls #
################

func _ready():
	if regenerate:
		generate( true )
	collect_submenu()
	if start_style == null:
		generate_styles()
	disable( disabled )
	restart()
	pass

func _process(delta):
	
	if !visible:
		return
	
	if current_style != null and target_style != null:
		if delta < 0.05 and not compare_styles( current_style, target_style ):
			mix_style( target_style, delta * target_style.speed )
			apply_style( current_style )

#####################
# events management #
#####################

# warning-ignore:unused_argument
# warning-ignore:unused_argument
func _input_event(viewport, event, shape_idx):
	
	if not reactive:
		return
	
	if event is InputEventMouseButton:
		if event.pressed and event.button_index == 1 and not disabled:
			if selectable:
				select( !selected )
			emit_signal( "hexa_pressed", self )

func _on_mouse_entered():
	
	if not reactive:
		return
	
	hovered = true
	if disabled or selected:
		return
	target_style = hover_style
	pass

func _on_mouse_exited():
	
	if not reactive:
		return
	
	hovered = false
	if disabled or selected:
		return
	target_style = normal_style
	pass

# recursive!
func hexa_connect( node, method ):
# warning-ignore:return_value_discarded
	self.connect( "hexa_pressed", node, method )
	for s in submenu:
		if s.is_hexa:
			s.node.hexa_connect( node, method )

######################
# submenu management #
######################

func collect_submenu():
	
	for c in get_children():
		if not c in subparts and c is CanvasItem:
			submenu.append( { 'node' : c, 'is_hexa': c.has_method( "collect_submenu" ) } )
			c.visible = submenu_visible
	if len( submenu ) > 6:
		printerr( "too many submenus! only 6 first will be taken into account" )
	
	# generate submenu positions:
	var start_at = 0
	if horizontal:
		start_at = 1
	var si = 0
	var rm = sin( PI / 3 )
	for i in range( start_at, 12, 2 ):
		if si >= len( submenu ):
			break 
		var a = TAU / 12 * i
		var r = radius * rm + gap
		if submenu[si].is_hexa:
			r += submenu[si].node.radius * rm
		else:
			r += radius * rm
		var v2 = Vector2( cos(a) * r, sin(a) * r )
		submenu[si].node.position = v2
		si += 1

func display_submenu( b ):
	submenu_visible = b
	for s in submenu:
		s.node.visible = submenu_visible
		if submenu_visible and s.is_hexa:
			s.node.restart()